from textflint.input_layer import Dataset
# load your data here, we use 5000 train data and 500 test data collected from IMDB dataset
train_dataset_path = 'IMDB_train.csv'
test_dataset_path = 'IMDB_test.csv'

# Dataset is defined to use TextFlint  more efficiently
train_data = Dataset(task='SA')
train_data.load_csv(train_dataset_path)

test_data = Dataset(task='SA')
test_data.load_csv(test_dataset_path)

import torch
import os
import numpy as np
from tqdm import tqdm
from torch.nn import functional as F
from textflint.common.utils import logger
from textflint.input_layer.model import FlintModel
from textflint.input_layer.model.test_model.textcnn_torch_model import TextCNNTorchModel
from textflint.input_layer.model.tokenizers.glove_tokenizer import GloveTokenizer
from textflint.input_layer.model.test_model.glove_embedding import GloveEmbedding
from textflint.input_layer.model.test_model.model_helper import *


class CNNFlint(FlintModel):
    r"""
    Model wrapper for TextCnn implemented by pytorch.

    """

    def __init__(self, model, tokenizer, label2id):
        super().__init__(
            model=model,
            task='SA',
            tokenizer=tokenizer
        )
        self.label2id = label2id

    def __call__(self, batch_texts):
        r"""
        Tokenize text, convert tokens to id and run the model.

        :param batch_texts: (batch_size,) batch text input
        :return: numpy.array()

        """
        model_device = next(self.model.parameters()).device
        inputs_ids = [self.encode(batch_text) for batch_text in batch_texts]
        ids = torch.tensor(inputs_ids).to(model_device)

        return self.model(ids).detach().cpu().numpy()

    def encode(self, inputs):
        r"""
        Tokenize inputs and convert it to ids.

        :param inputs: model original input
        :return: list of inputs ids

        """
        return self.tokenizer.encode(inputs)

    def unzip_samples(self, data_samples):
        r"""
        Unzip sample to input texts and labels.

        :param list[Sample] data_samples: list of Samples
        :return: (inputs_text), labels.

        """
        x = []
        y = []

        for sample in data_samples:
            x.append(sample['x'])
            y.append(self.label2id[sample['y']])

        return [x], y


def train(training_data, args):
    '''
    Train process for TextCNN, create a TextCNN model and train it with training_data and args
    '''

    model = TextCNNTorchModel(init_embedding=args['embedding'].embedding)
    model.train()
    optimizer = torch.optim.Adam(model.parameters(), lr=args['lr'])
    #epoch_bar = tqdm(range(1, args['epoch'] + 1),disable=True)
    for epoch in tqdm(range(1, args['epoch'] + 1)):
        total_loss = 0
        steps = 0
        corrects = 0
        for inputs, labels in train_iter(training_data, args['batch_size'], args['tokenizer'], args['label2id']):
            inputs, labels = torch.from_numpy(inputs).to(args['device']), torch.from_numpy(labels).to(args['device'])
            optimizer.zero_grad()
            logits = model(inputs)

            loss = F.cross_entropy(logits, labels)
            total_loss += loss.item()
            loss.backward()
            optimizer.step()

            result = torch.max(logits, 1)[1].view(labels.size())
            steps += 1
            corrects += (result.data == labels.data).sum()
        accuracy = corrects * 100.0 / (args['batch_size'] * steps)
        #         epoch_bar.set_description('epoch {}'.format(epoch))
        #epoch_bar.set_description('loss({:.2f}) train acc({:.2f})'.format(total_loss, accuracy))
        print('loss({:.2f}) train acc({:.2f})'.format(total_loss, accuracy))
    #     print(f"epoch {epoch}\t loss {total_loss} train acc {accuracy}")

    return model


# Essential parts(embedding, tokenizer) and args for training TextCNN
glove_embedding = GloveEmbedding()
tokenizer = GloveTokenizer(
    word_id_map=glove_embedding.word2id,
    unk_token_id=glove_embedding.oovid,
    pad_token_id=glove_embedding.padid,
    max_length=512
)


label2id = {"negative": 0, "positive": 1}
train_args = {'lr': 0.0001, 'epoch': 30, 'batch_size': 64, 'device': torch.device("cuda" if torch.cuda.is_available() else "cpu"), 'max_length': 512, 'label2id': label2id,
              'tokenizer': tokenizer, 'embedding': glove_embedding}

cnn_model = train(training_data=train_data.dump(), args=train_args)

# Wrapper trained model with CNNFlint
cnn_flint = CNNFlint(model=cnn_model, tokenizer=tokenizer, label2id=label2id)

from textflint.engine import Engine
from textflint.adapter import auto_config
from textflint.input_layer.config import Config
from textflint.common.settings import UNMATCH_UT_TRANSFORMATIONS, \
    TASK_TRANSFORMATIONS

# Config for TextFlint to transform data, see more details in https://textflint.readthedocs.io/en/latest/
config = auto_config(task='SA')
config.trans_methods = ['DoubleDenial', 'Ocr']
config.sub_methods = []
engine = Engine()
# Generate adversarial train data, saved in ./train_results dir
config.out_dir = 'result/train_results'
#engine.run(data_input=train_dataset_path, config=config)

# Generate adversarial test data, saved in ./test_results dir
config.out_dir = 'result/test_results'
#engine.run(data_input=test_dataset_path, config=config)

import re
def find_target_data(trans_name, output_dir, prefix='trans'):
    '''
    Find target data generated by TextFlint in output_dir
    '''
    file_list = os.listdir(output_dir)
    for file in file_list:
        if re.match('{}_{}_\d+.json'.format(prefix, trans_name), file):
            return '{}/{}'.format(output_dir, file)
    return None

def compare_evaluate(flintmodel, trans_name, output_dir='result/test_results'):
    '''
    Evaluate flintmodel in original test data and transformed test data
    '''
    ori_test_data = Dataset(task='SA')
    ori_test_data.load_json(find_target_data(trans_name=trans_name, output_dir=output_dir, prefix='ori'))
    trans_test_data = Dataset(task='SA')
    trans_test_data.load_json(find_target_data(trans_name=trans_name, output_dir=output_dir, prefix='trans'))

    flintmodel.model.eval()
    ori_eval_result = flintmodel.evaluate(ori_test_data.dump(), prefix='{}_ori_'.format(trans_name))
    trans_eval_result = flintmodel.evaluate(trans_test_data.dump(), prefix='{}_trans_'.format(trans_name))
    return ori_eval_result, trans_eval_result

ori_model_results = []

# Evaluate cnn_flint in original test data and transformed test data.
for trans_method in config.trans_methods:
#     print('Evaluate the performence of TextCNN {}'.format(trans_method))
    cnn_results = compare_evaluate(cnn_flint, trans_method)
    ori_model_results.append(cnn_results)
#     print('Adversarial training for TextCNN {}'.format(trans_method))

print(ori_model_results)

import re


def adv_train(trans_name, output_dir='result/train_results'):
    '''
    Adversarial training for TextCNN and target transformation
    '''
    # Mix up original training data and adversarial sample
    mix_train_data = Dataset(task='SA')
    mix_train_data.load_json(find_target_data(trans_name=trans_name, output_dir=output_dir))
    mix_train_data.load_csv(train_dataset_path)

    # retrain TextCNN with mixed data and evaluate it in original test data and transformed test data
    adv_cnn_model = train(training_data=mix_train_data.dump(), args=train_args)
    adv_cnn_flint = CNNFlint(model=adv_cnn_model, tokenizer=tokenizer, label2id=label2id)
    return adv_cnn_flint


adv_model_results = []

# adversarial training for cnn_flint, and evaluate new model in original test data and transformed data.
for trans_method in config.trans_methods:
    adv_cnn_flint = adv_train(trans_method)
    adv_cnn_result = compare_evaluate(adv_cnn_flint, trans_method)
    adv_model_results.append(adv_cnn_result)

# Compare the performance of original model and adversarial training model
print('-----------------Original TextCNN model results-----------------')
print(ori_model_results)
print('-----------------TextCNN with adversarial training results-----------------')
print(adv_model_results)

